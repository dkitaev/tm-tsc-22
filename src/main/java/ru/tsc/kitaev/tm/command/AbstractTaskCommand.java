package ru.tsc.kitaev.tm.command;

import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

}
