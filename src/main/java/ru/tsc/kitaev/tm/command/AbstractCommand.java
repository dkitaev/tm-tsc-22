package ru.tsc.kitaev.tm.command;

import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    public Role[] roles() {
        return null;
    }

    @Override
    public String toString() {
        String result = "";
        String name = name();
        String description = description();
        if (name != null || !name.isEmpty()) result += name + " ";
        if (description != null || !description.isEmpty()) result += ":" + description;
        return result;
    }

}
