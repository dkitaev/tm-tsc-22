package ru.tsc.kitaev.tm.repository;

import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    public static Predicate<Task> predicateByName(final String name) {
        return s -> name.equals(s.getName());
    }

    public static Predicate<Task> predicateByProjectId(final String projectId) {
        return s -> projectId.equals(s.getProjectId());
    }

    public static Predicate<Task> predicateByTaskId(final String taskId) {
        return s -> taskId.equals(s.getId());
    }

    @Override
    public Task findByName(final String userId, final String name) {
        return findAll(userId).stream()
                .filter(predicateByName(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(this::remove);
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task startById(final String userId, final String id) {
        final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task startByIndex(final String userId, final Integer index) {
        final Optional<Task> task = Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task startByName(final String userId, final String name) {
        final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task finishById(final String userId, final String id) {
        final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task finishByIndex(final String userId, final Integer index) {
        final Optional<Task> task = Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        final Optional<Task> task = Optional.ofNullable(findById(userId, id));
        task.ifPresent(p -> p.setStatus(status));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final Optional<Task> task = Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(p -> p.setStatus(status));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task changeStatusByName(final String userId, final String name, final Status status) {
        final Optional<Task> task = Optional.ofNullable(findByName(userId, name));
        task.ifPresent(p -> p.setStatus(status));
        return task.orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task findByProjectIdAndTaskId(final String userId, final String projectId, final String taskId) {
        return findAll(userId).stream()
                .filter(predicateByProjectId(projectId))
                .filter(predicateByTaskId(taskId))
                .findFirst()
                .orElse(null);
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, final String projectId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public Task bindTaskToProjectById(final String userId, final String projectId, final String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String userId, final String projectId, final String taskId) {
        final Task task = findByProjectIdAndTaskId(userId, projectId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String projectId) {
        final List<Task> listByProject = findAllTaskByProjectId(userId, projectId);
        list.removeAll(listByProject);
    }

}
