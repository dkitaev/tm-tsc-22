package ru.tsc.kitaev.tm.repository;

import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.Project;

import java.util.Optional;
import java.util.function.Predicate;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    public static Predicate<Project> predicateByName(final String name) {
        return s -> name.equals(s.getName());
    }

    @Override
    public Project findByName(final String userId, final String name) {
        return findAll(userId).stream()
                .filter(predicateByName(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(this::remove);
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project startById(final String userId, final String id) {
        final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project startByIndex(final String userId, final Integer index) {
        final Optional<Project> project = Optional.ofNullable(findByIndex(userId, index));
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project startByName(final String userId, final String name) {
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project finishById(final String userId, final String id) {
        final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project finishByIndex(final String userId, final Integer index) {
        final Optional<Project> project = Optional.ofNullable(findByIndex(userId, index));
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project finishByName(final String userId, final String name) {
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) {
        final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> p.setStatus(status));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final Optional<Project> project = Optional.ofNullable(findByIndex(userId, index));
        project.ifPresent(p -> p.setStatus(status));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project changeStatusByName(final String userId, final String name, final Status status) {
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(p -> p.setStatus(status));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

}
