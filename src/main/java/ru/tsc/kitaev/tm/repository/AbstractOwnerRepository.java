package ru.tsc.kitaev.tm.repository;

import ru.tsc.kitaev.tm.api.repository.IOwnerRepository;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    public static Predicate<AbstractOwnerEntity> predicateByUserId (final String userId) {
        return s -> userId.equals(s.getUserId());
    }

    @Override
    public E add(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return null;
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return;
            list.remove(entity);
    }

    @Override
    public void clear(final String userId) {
        final List<E> entity = findAll(userId);
        list.removeAll(entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return findAll(userId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String userId, final String id) {
        return findAll(userId).stream()
                .filter(predicateById(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        final List<E> entities = findAll(userId);
        return entities.get(index);
    }

    @Override
    public E removeById(final String userId, final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId,id));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId,index));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        final E entity = findById(userId, id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(final String userId, final int index) {
        final E entity = findByIndex(userId, index);
        return entity != null;
    }

    @Override
    public Integer getSize(final String userId) {
        return (int) list.stream()
                .filter(predicateByUserId(userId))
                .count();
    }

}
