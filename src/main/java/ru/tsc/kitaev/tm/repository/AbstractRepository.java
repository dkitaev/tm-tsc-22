package ru.tsc.kitaev.tm.repository;

import ru.tsc.kitaev.tm.api.repository.IRepository;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AbstractRepository <E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    public static Predicate<AbstractEntity> predicateById(final String id) {
        return s -> id.equals(s.getId());
    }

    @Override
    public E add(final E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        list.remove(entity);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        return list.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String id) {
        return list.stream()
                .filter(predicateById(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public E findByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public E removeById(final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E removeByIndex(final Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(index));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsById(final String id) {
        final E entity = findById(id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(final int index) {
        if (index < 0) return false;
        return index < list.size();
    }

    @Override
    public Integer getSize() {
        return list.size();
    }

}
