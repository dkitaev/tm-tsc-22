package ru.tsc.kitaev.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
